package com.universe.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TennisCoach implements Coach {

	private FortuneService fortuneService;
	
	//define a default constructor
	public TennisCoach() {
		System.out.println("inside Default constructor : TennisCoach");
	}
	
	/*
	//define a setter method
	@Autowired
	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("inside setter method : TennisCoach");
		this.fortuneService = fortuneService;
	}
	*/
	//define a setter method
	@Autowired
	public void doTheMagicSpring(FortuneService fortuneService) {
		System.out.println("inside setter method : TennisCoach");
		this.fortuneService = fortuneService;
	}
	
	/*
	@Autowired
	public TennisCoach(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}
	*/
	
	@Override
	public String getDailyWorkout() {
		return "Practice your forehand volley";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}
	
	

}
