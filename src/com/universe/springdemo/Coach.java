package com.universe.springdemo;

public interface Coach {
	
	public String getDailyWorkout();
	
	public String getDailyFortune();

}
